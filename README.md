# Getting a Manila Shares
This provides a recipie to create shares and connect hosts to the share in your Project


## Setup your environment
You need to install the manila client, `python-manilaclient`, to manage shared filesystems with your OpenStack CLI. This could be via PIP:
```sh
pip install --user python-manilaclient
```
alternative via your package manager, e.g. with APT:
```sh
sudo apt install -y python3-manilaclient
```
Now you have the tools to proceed.


## NFS based shares
The easiest way to get a shared filesystem from OpenStack is via NFS. All clients of the share use a single thread of a Ganesha NFS, meaning it will run into performance issues with busy file systems. For higher performance consider using CephFS based shares or NexusPOSIX.

To get started request access to NFS based shares on the helpdesk. We will grant you access to the `manila-nfs` network.


### HEAT based recipie
The file `nfs-share.yaml` provides a HEAT template to create a share, create two instances, and install the nfs client on those instances. To set up the demo use:
```
openstack stack create nfs-demo -t nfs-share.yaml --parameter key_name=KEY_NAME --wait
```
where KEY_NAME is the name of an SSH key you uploaded to OpenStack with `openstack keypair create ...` as explained in our [quickstart](https://docs.mpcdf.mpg.de/doc/cloud/technical/quickstart.html).

The stack provides the IP addesses of the instances and the export location of the share in its outputs, you can view it like this:
```
openstack stack output show nfs-demo --all
```
SSH to the clients and mount the share using the export_location, like so:
```
mount -t nfs \
    172.17.0.172:/volumes/_nogroup/... \
    /share
```


### Step-by-step
Create the share:
```sh
openstack share create NFS 1 \
    --name nfs-share \
    --share-type CephNFS
```
You can look up the export path of the share with:
```sh
openstack share show nfs-share -c export_locations
```
You can use the path to automate the setup of the client in a `user-data.yaml` file, then create some clients with:
```sh
openstack server create client \
    --image "Debian 12" \
    --flavor mpcdf.tiny \
    --key KEY_NAME \
    --network cloud-local-3 \
    --network manila-nfs \
    --user-data user-data.yaml \
    --min 2 --max 3
```
Look up the IP addresses of yoru servers using on the `manila-nfs` network:
```
openstack server show SERVER_NAME -c addresses
```
then allow the server access to the share:
```
openstack share access create nfs-share ip IP
```
Now you can SSH into the server using the IP from the cloud-local-3 network and mount the nfs share:
```
systemctl daemon-reload
mount /mnt
```


## CephFS
CephFS shares promise better performance but imply more configuration effort: the CephFS clients running on your VM must be compatible with the ceph version we are running.

To get started request access to CephFS based shares on the helpdesk. We will grant you access to the `manila-nfs` network.


### Step-by-Step
Setup the share and request access for a user
```sh
SIZE=1  # share size in GiB
SHARE="cephfs-demo"  # share name
CEPHUSER="demo-user"  # user name to authenticate to ceph later

openstack share create CEPHFS $SIZE --share-type CephNative --name $SHARE
openstack share access create $SHARE cephx $CEPHUSER
```

Wait a moment for the share to be created, and Ceph to communicate the credentials back to OpenStack, then:
```sh
openstack share access list $SHARE
```
Note down the access key, together with the `CEPHUSER` these are the credentials needed to connect to the share. Finally we need the location of the share on Ceph to mount, get it using:
```sh
openstack share show $SHARE -c export_locations
```
The relevant part of the output is is the part of the path after the `:`, it should look likeathe bold part of this: 
<pre>
+------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Field            | Value                                                                                                                                                                                                  |
+------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| export_locations |                                                                                                                                                                                                        |
|                  | id = 8566fc0a-3955-427e-9bdd-e50d43623f02                                                                                                                                                              |
|                  | path = 10.186.136.211:6789,10.186.136.212:6789,10.186.136.213:6789,10.186.136.214:6789,10.186.136.215:6789<b>:/volumes/_nogroup/e7ff08a7-ff0d-42fc-a9ea-271864bc81df/ebb5cdfd-28e6-47be-8d01-3c3acefd7567</b> |
|                  | preferred = False                                                                                                                                                                                      |
+------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
</pre>

Add the `manila-cephfs` network to an existing server:
```sh
openstack server add network $SERVER manila-cephfs
```
**or** create some servers with the networks `cloud-local-3` for SSH access and `manila-cfs` to mount the share:
```sh
KEY_NAME=<ssh-keypair-name>   # name of an ssh keypair you previously added to OpenStack
openstack server create client \
    --image "Debian 12" \
    --flavor mpcdf.tiny \
    --key $KEY_NAME \
    --network cloud-local-3 \
    --network manila-cephfs \
    --min 2 --max 3
```

Now configure the client server to mount the shared filesystem, on the clients do:
```sh
CEPHUSER="demo-user"  # set at the versy start
ACCESS_KEY="XXX"  # retrieved from the share access above
EXPORT_PATH=":/volumes/_nogroup/..."  # retrieved from the share above

apt install -y ceph-common
cat > /etc/ceph/ceph.conf <<EOF
[global]
    mon_host = manila-cephfs.hpccloud.mpcdf.mpg.de
EOF
umask 177
cat >> /etc/ceph/keyring <<EOF
[client.$CEPHUSER]
    key = $ACCESS_KEY
EOF
umask 022
echo "$EXPORT_PATH /share ceph name=$CEPHUSER,ms_mode=secure,_netdev 0 0" >> /etc/fstab
mkdir /share && mount /share
```

### HEAT Recipie
The file `cephfs-share.yaml` provides a HEAT template to create a share, create two instances, and install the nfs client on those instances. To set up the demo use:
```
openstack stack create cephfs-demo -t cephfs-share.yaml --parameter key_name=KEY_NAME --wait
```
where KEY_NAME is the name of an SSH key you uploaded to OpenStack with `openstack keypair create ...` as explained in our [quickstart](https://docs.mpcdf.mpg.de/doc/cloud/technical/quickstart.html).

The stack provides the IP addesses of the instances and the export location of the share in its outputs, you can view it like this:
```
openstack stack output show cephfs-demo --all
```
You will also need to look up the `access_key` for the share created by this template manually:
```sh
openstack share access list cephfs-demo -c "Access Key" -f value
```
SSH to the clients add the export location details in the `/etc/fstab` and  and the access key to `/etc/ceph/ceph.client.demo-user.keyring`. With that you can mount the CephFS share with
```
mount /share
```